# trajectory_predict_learning

#### 介绍
本仓库仅仅是对轨迹预测的传统算法的学习，不做任何商业用途，如果引用，请引用原算法的论文。
比如Fast Tracker中的基于贝塞尔曲线的轨迹预测算法。如果引用请引用原仓库和论文。

#### 软件架构
软件架构说明


#### 安装教程
In **target prediction** part, we use **OOQP** for quadratic programming.

1. Type the following commands to install dependencies.

   ```
   sudo apt-get install gfortran
   sudo apt-get install doxygen
   ```

2. Get a copy of **MA27** from the [HSL Archive](http://www.hsl.rl.ac.uk/download/MA27/1.0.0/a/). Just select the **Personal Licence (allows use without redistribution)**, then fill the information table. 

​        Then you can download it from an e-mail sent to you. Next, un-zip **MA27**, and follow the *README* in it, install it to your Ubuntu.

**Actually, you only need to type 3 commands in MA27's folder to finish the installation.**

```
./configure
make
sudo make install
```

3. Manually un-zip packages *OOQP.zip* in the repo and install it to your Ubuntu.

**As above, you can just type 3 commands in OOQP's folder :**

```
./configure
make 
sudo make install
```

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 问题
1.生成动态库出现的问题

      add_library(bezier_predict_python SHARED
      src/bezier_predict_python.cpp
      src/bezier_predict.cpp                 ##这里加入了前一个cpp文件中已经include了的头文件的原文件，这里就会报错。生成不了.so动态库。而python调用.a静态库会报错  这是因为没有源文件，ooqp的相应函数就不编译了。而实际就是这个文件就是不能编译为.a文件
      )
      target_link_libraries(bezier_predict_python
      ${catkin_LIBRARIES}
      ${PCL_LIBRARIES}
      ooqpgensparse 
      ooqpsparse 
      ooqpgondzio 
      ooqpbase blas ma27 gfortran 
      )

可能会导致的另一个问题就是，出现函数重复定义问题  

解决办法  
将已经include的源文件cpp，不要加入其中。  
如果仅仅inculde头文件的话，将会出现未定义的引用。因为就是有函数没有定义。这时就需要将相应的.cpp源文件加入到    

      add_executable(bezier_predict_thread
            src/bezier_predict_thread.cpp
            src/bezier_predict.cpp
            )

